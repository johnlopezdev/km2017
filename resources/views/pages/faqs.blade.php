@extends('layout.master')
@section('content')
<!--========== BREADCRUMBS V5 ==========-->
<section class="breadcrumbs-v5 breadcrumbs-v5-bg-img-v2 content-lg">
    <div class="container">
        <h2 class="breadcrumbs-v5-title">Frequently Asked Questions</h2>
        <h3 class="color-white">We don't have a uniform, belts or any indicator of rank or seniority.</h3>
    </div>
</section>
<!--========== END BREADCRUMBS V5 ==========-->

<!-- FAQ Content -->
<div class="bg-color-sky-light">
    <div class="content-md container">
        <div class="row">
            <div class="col-md-12 md-margin-b-50">
                <!-- Accordrion v5 -->
                <div class="accordion-v5 accordion-v5-active-left">
                    <div class="panel-group" id="accordion-v5" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion-v5" href="#accordionV5CollapseOne" aria-expanded="true" aria-controls="accordionV5CollapseOne">
                                        <span class="h3 fweight-300">I've never studied any martial art. Will I be able to study Filipino Martial Arts?</span>
                                    </a>
                                </h4>
                            </div>
                            <div id="accordionV5CollapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body font-size-18 line-height-2">
                                    You don't need to have prior experience studying another martial art. What we do require is the willingness to
                                    learn and seriously train, and the good moral foundation to guide the skills that will be learned.
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-v5" href="#accordionV5CollapseTwo" aria-expanded="false" aria-controls="accordionV5CollapseTwo">
                                        <span class="h3 fweight-300">What can I expect during my first class at Kali Manila?</span>
                                    </a>
                                </h4>
                            </div>
                            <div id="accordionV5CollapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body font-size-18 line-height-2">
                                    All beginners are required to go through the Beginners Program. This introductory course is intended to teach the
                                    basic strikes and footwork. The goal is for the new student to gain an understanding of the basic skill before
                                    joining the rest of the club for technical training. The basics skills will gradually be sharpened into functional
                                    skills and instincts as the student trains in the regular class. Progress of the student in training is completely
                                    up to him or her; instead of providing ranks or belt to designate progress, we believe that the student has to judge
                                    himself or herself against his or her peers by making their own personal, honest and unbiased assessment of
                                    the skills learned.
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-v5" href="#accordionV5CollapseThree" aria-expanded="false" aria-controls="accordionV5CollapseThree">
                                        <span class="h3 fweight-300">What do I have to wear when I start training? Do I need to buy a uniform?</span>
                                    </a>
                                </h4>
                            </div>
                            <div id="accordionV5CollapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                <div class="panel-body font-size-18 line-height-2">
                                    We don't use uniforms in our organization. We don't even have belts or any indicator of rank or seniority. We train in
                                    regular exercise clothing such as t-shirts, shorts, trainers, etc. We do have a club shirt that most of our members use
                                    in class, which is as close as we have to a "uniform".
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingFour">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-v5" href="#accordionV5CollapseFour" aria-expanded="false" aria-controls="accordionV5CollapseFour">
                                        <span class="h3 fweight-300">What do I need to bring for my first day of training?</span>
                                    </a>
                                </h4>
                            </div>
                            <div id="accordionV5CollapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                                <div class="panel-body font-size-18 line-height-2">
                                    Nothing much, just bring enough drinking water for strenuous training. We can provide the training weapons for beginners.
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingFive">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-v5" href="#accordionV5CollapseFive" aria-expanded="false" aria-controls="accordionV5CollapseFive">
                                        <span class="h3 fweight-300">Are classes conducted in English or Filipino?</span>
                                    </a>
                                </h4>
                            </div>
                            <div id="accordionV5CollapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                                <div class="panel-body font-size-18 line-height-2">
                                    If the class has foreign students, English is used throughout the class. If the class does not have any foreign students, Filipino and English is used.
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingSix">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-v5" href="#accordionV5CollapseSix" aria-expanded="false" aria-controls="accordionV5CollapseSix">
                                        <span class="h3 fweight-300">Do you have knife fighting classes, or sessions specifically for training with knives?</span>
                                    </a>
                                </h4>
                            </div>
                            <div id="accordionV5CollapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
                                <div class="panel-body font-size-18 line-height-2">
                                    We do knife training as part of regular classes. Stick practice - and the body movement and the understanding of angles of attack that it teaches
                                    the student - is the basis of all weapon and even non-weapon training in Filipino Martial Arts. So in a sense we are always doing knife
                                    training. When we do conduct knife-specific training, it is limited to only intermediate and advanced students.
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingSeven">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-v5" href="#accordionV5CollapseSeven" aria-expanded="false" aria-controls="accordionV5CollapseSeven">
                                        <span class="h3 fweight-300">What's the point of training to use a weapon? I don't have a stick with me all the time.</span>
                                    </a>
                                </h4>
                            </div>
                            <div id="accordionV5CollapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">
                                <div class="panel-body">
                                    <p class="font-size-18 line-height-2 margin-b-30">
                                        Having a weapon - matched with the skills to utilize it to the fullest - always gives the defender a crucial advantage over
                                        an empty-handed attacker. Against an armed attack, having a weapon will improve the odds for the otherwise empty-handed defender.
                                    </p>
                                    <p class="font-size-18 line-height-2 margin-b-30">
                                        In Filipino Martial Arts, the stick represents a long bladed weapon like a bolo or machete. The stick can easily be replaced by
                                        anything at hand, regardless of its length or shape. So you don't just train to just use a stick or a knife; you train to use
                                        anything at hand as an improvised weapon.
                                    </p>
                                    <p class="font-size-18 line-height-2 margin-b-30">
                                        We are almost always surrounded by potential weapons or you already have one on you. Ashtrays, cellphones, pens, books and
                                        just about anything you can grab and use can be a weapon. Filipino Martial Arts weapons training is directly applicable to
                                        anything you can pick up and use as a potential weapon. The mentality of seeing everything as having the characteristics
                                        of a potential weapon, and yet having skills to defend yourself empty-handed if needed, is a major advantage in a self-defense situation.
                                    </p>
                                    <p class="font-size-18 line-height-2 margin-b-30">
                                        Unlike other martial arts systems where empty-handed skills are taught before weapons, the Filipino Martial Arts trains with weapons first
                                        and uses movement, speed, reflexes, instincts and other attributes to lead to empty-handed skills.
                                    </p>
                                    <p class="font-size-18 line-height-2 margin-b-30">
                                        We believe that the most important component in a self-defense situation is the individual and his or her determination to survive. This is
                                        why we often say that a student of the Filipino Martial Arts  is training to BE a weapon, rather than be completely dependent on having one at hand.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingEight">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-v5" href="#accordionV5CollapseEight" aria-expanded="false" aria-controls="accordionV5CollapseEight">
                                        <span class="h3 fweight-300">So the Filipino Martial Arts are not only about weapons?</span>
                                    </a>
                                </h4>
                            </div>
                            <div id="accordionV5CollapseEight" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEight">
                                <div class="panel-body font-size-18 line-height-2">
                                    Yes. What we teach has its own empty hand subsystem, based on the same body movement developed with weapons training.
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingNine">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-v5" href="#accordionV5CollapseNine" aria-expanded="false" aria-controls="accordionV5CollapseNine">
                                        <span class="h3 fweight-300">Do you accept requests for classes focusing on knife training only?</span>
                                    </a>
                                </h4>
                            </div>
                            <div id="accordionV5CollapseNine" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingNine">
                                <div class="panel-body font-size-18 line-height-2">
                                    No.
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTen">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-v5" href="#accordionV5CollapseTen" aria-expanded="false" aria-controls="accordionV5CollapseTen">
                                        <span class="h3 fweight-300">Why should I train to use knives? I have no intention of carrying one anyway.</span>
                                    </a>
                                </h4>
                            </div>
                            <div id="accordionV5CollapseTen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTen">
                                <div class="panel-body">
                                    <p class="font-size-18 line-height-2 margin-b-30">
                                        A vast majority of armed robberies and assaults in the Philippines involve the use of an edged weapon. Knife training is meant to give the student a realistic, instinctive and thorough understanding of how a knife is used.
                                    </p>
                                    <p class="font-size-18 line-height-2 margin-b-30">
                                        The chance of surviving empty-handed against a vicious knife attack is already slim; knowledge and experience with how the weapon is used is essential if one even hopes to survive such an attack. You cannot defend against a weapon or type of attack if you haven't experienced using it yourself. The Filipino Martial Arts are well known for teaching realistic and practical weapon skills and our particular emphasis is on edged weapons.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingEleven">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion-v5" href="#accordionV5CollapseEleven" aria-expanded="false" aria-controls="accordionV5CollapseEleven">
                                        <span class="h3 fweight-300">I've never heard of Filipino Martial Arts. Why is that?</span>
                                    </a>
                                </h4>
                            </div>
                            <div id="accordionV5CollapseEleven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEleven">
                                <div class="panel-body">
                                    <p class="font-size-18 line-height-2 margin-b-30">
                                        Arnis, Escrima and Kali aka Filipino Martial Arts have always had an excellent reputation for being realistic and
                                        practical, making them a staple of police and military training here and abroad. Compared with most other
                                        martial arts, FMA in general remains very closely linked to realistic combat.
                                    </p>
                                    <p class="font-size-18 line-height-2 margin-b-30">
                                        Ironically, the effectiveness of FMA has been largely ignored or unappreciated by Filipinos over the years in
                                        favor of foreign martial arts. Nowadays foreigners are more aware and appreciative of FMA than most Filipinos.
                                        The fact that FMA are well rounded fighting systems - beyond being simply about stick fighting - is even
                                        less understood by Filipinos.
                                    </p>
                                    <p class="font-size-18 line-height-2 margin-b-30">
                                        However, the popularity of FMA is picking up, due to its use in numerous movies. Elements of FMA have been
                                        used in films for years, from Bruce Lee's "Enter The Dragon" to the Bourne series. Filipino Martial Arts
                                        was also used in "Repo Men","Blade II","Book of Eli","300" (as the fighting style of the Spartans),
                                        "Ultraviolet", "Daredevil", "Chronicles of Riddick", "Equilibrium" and "Matrix: Reloaded".
                                    </p>
                                    <p class="font-size-18 line-height-2 margin-b-30">
                                        Despite FMA's increasing use in movies, it is often mistaken for other martial arts styles by the the
                                        general public. A good example is the common misconception that Krav Maga was used in the fight scenes
                                        of the Bourne movies. Actually, FMA was used as the fighting system used by the Jason Bourne character in
                                        all three movies. Matt Damon describes his FMA training in the DVD special features of one of the movies.
                                        Jeff Imada, a well known FMA teacher and student of Dan Inosanto, was the fight choregrapher of the Bourne series.
                                    </p>
                                    <p class="font-size-18 line-height-2 margin-b-30">
                                        The Internet has helped a lot with making Filipinos more aware of their rich martial heritage, providing valuable
                                        information on local FMA groups, making the systems easier to find.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Accordrion v5 -->
            </div>
        </div>
    </div>
</div>
<!-- End FAQ Content -->
@endsection