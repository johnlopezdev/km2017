@extends('layout.master')
@section('content')

<!-- Blog Teaser -->
<div class="container margin-b-80">
    <div class="row margin-t-50">
        <div class="col-md-9">
            <!-- Blog Teaser -->
            <article class="blog-teaser margin-b-50">
                <img class="img-responsive" src="assets/img/1920x1080/23.jpg" alt="">
                <div class="blog-teaser-overlay">
                    <div class="blog-teaser-center-align">
                        <h2 class="blog-teaser-title">Wood Shed</h2>
                        <p class="blog-teaser-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.</p>
                    </div>
                    <ul class="list-inline blog-teaser-category xs-hidden">
                        <li class="blog-teaser-category-title">Strategy</li>
                        <li class="blog-teaser-category-title">Branding</li>
                        <li class="blog-teaser-category-title">Lifestyle</li>
                    </ul>
                </div>
                <a class="blog-teaser-link" href="{{ route('article', ['id' => 1]) }}"></a>
            </article>
            <!-- End Blog Teaser -->

            <!-- Blog Teaser -->
            <article class="blog-teaser margin-b-50">
                <img class="img-responsive" src="assets/img/1920x1080/24.jpg" alt="">
                <div class="blog-teaser-overlay">
                    <div class="blog-teaser-center-align">
                        <h2 class="blog-teaser-title">Century of Hi-Tech</h2>
                        <p class="blog-teaser-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.</p>
                    </div>
                    <ul class="list-inline blog-teaser-category xs-hidden">
                        <li class="blog-teaser-category-title">Strategy</li>
                        <li class="blog-teaser-category-title">Branding</li>
                        <li class="blog-teaser-category-title">Lifestyle</li>
                    </ul>
                </div>
                <a class="blog-teaser-link" href="#"></a>
            </article>
            <!-- End Blog Teaser -->

            <!-- Blog Teaser -->
            <article class="blog-teaser margin-b-50">
                <img class="img-responsive" src="assets/img/1920x1080/25.jpg" alt="">
                <div class="blog-teaser-overlay">
                    <div class="blog-teaser-center-align">
                        <h2 class="blog-teaser-title">Ark Cycle Share</h2>
                        <p class="blog-teaser-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.</p>
                    </div>
                    <ul class="list-inline blog-teaser-category xs-hidden">
                        <li class="blog-teaser-category-title">Strategy</li>
                        <li class="blog-teaser-category-title">Branding</li>
                        <li class="blog-teaser-category-title">Lifestyle</li>
                    </ul>
                </div>
                <a class="blog-teaser-link" href="{{ route('article', ['id' => 1]) }}"></a>
            </article>
            <!-- End Blog Teaser -->

            <!-- Blog Teaser -->
            <article class="blog-teaser">
                <img class="img-responsive" src="assets/img/1920x1080/26.jpg" alt="">
                <div class="blog-teaser-overlay">
                    <div class="blog-teaser-center-align">
                        <h2 class="blog-teaser-title">Watson Furniture</h2>
                        <p class="blog-teaser-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.</p>
                    </div>
                    <ul class="list-inline blog-teaser-category xs-hidden">
                        <li class="blog-teaser-category-title">Strategy</li>
                        <li class="blog-teaser-category-title">Branding</li>
                        <li class="blog-teaser-category-title">Lifestyle</li>
                    </ul>
                </div>
                <a class="blog-teaser-link" href="{{ route('article', ['id' => 1]) }}"></a>
            </article>
            <!-- End Blog Teaser -->
        </div>
        <!--========== BLOG SIDEBAR ==========-->
        <div class="col-md-3">
            @include('layout.widgets.blog-sidebar')
        </div>
        <!--========== END BLOG SIDEBAR ==========-->
    </div>
</div>
<!-- End Blog Teaser -->

@endsection