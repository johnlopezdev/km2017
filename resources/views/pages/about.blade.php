@extends('layout.master')
@section('content')
<!--========== BREADCRUMBS V5 ==========-->
<section class="breadcrumbs-v5 content-lg">
    <div class="container">
        <h2 class="breadcrumbs-v5-title">About Us</h2>
        <h3 class="color-white">Everything you need to know about Kali Manila</h3>
    </div>
</section>
<!--========== END BREADCRUMBS V5 ==========-->

<!-- News v6 -->
<div class="bg-color-sky-light">
    <div class="content-md container-sm">
        <!-- News v6 -->
        <div class="row padding-lr-20 margin-b-80 md-margin-b-0">
            <div class="col-md-6 no-space md-margin-b-30">
                <div class="news-v6 margin-t-50 margin-r-o-50">
                    <div class="margin-b-30">
                        <h3 class="news-v6-title text-uppercase">Formerly known as PTK Manila</h3>
                    </div>
                    <p class="font-size-18 margin-b-30 line-height-2">Kali Manila is the latest name for a group that began in June of 2006, when it was organized as the Pekiti Tirsia Kali Manila (or PTK Manila). However the group has been in existence in various forms since 2003, and has its roots as the training area of the first Pekiti Tirsia Kali group in Metro Manila which started their training under Tuhon Rommel Tortal at the campus of the University of the Philippines in 2002.</p>
                    <p class="font-size-18 margin-b-30 line-height-2">The club's members and instructors participated and assisted in various Pekiti Tirsia training courses for both local and foreign military and police units conducted in the Philippines. They were featured as part of the cast of the Filipino Martial Arts episode of the "Fight Quest" series and the BBC's "Mind, Body And Kick Ass Moves". The club was a founding member of the Pekiti Tirsia Kali Asia Pacific Confederation.</p>
                </div>
            </div>
            <div class="col-md-6 no-space wow fadeInUp" data-wow-duration=".2" data-wow-delay=".1s">
                <div class="padding-30 bg-color-white">
                    <img class="img-responsive" src="https://pbs.twimg.com/profile_images/472354149799976960/2wq2Xii2.png" alt="">
                </div>
            </div>
        </div>
        <!-- End News v6 -->

        <!-- News v6 -->
        <div class="row padding-lr-20 margin-b-80 md-margin-b-0">
            <div class="col-md-6 col-md-push-6 no-space md-margin-b-30">
                <div class="news-v6 margin-t-50 margin-l-o-50">
                    <div class="margin-b-20">
                        <h3 class="news-v6-title text-uppercase">Leadership and Development</h3>
                    </div>
                    <p class="font-size-18 margin-b-30 line-height-2">
                        PTK Manila was the first civilian PTK school established in Metro Manila and was one of the largest organizations of the system in the city.
                        In 2007, the leadership of the club was passed on to
                        <a href="Mandala Buddy Acenas"></a>, who is currently the Chief Instructor of Kali Manila.
                    </p>
                    <p class="font-size-18 margin-b-30 line-height-2">
                        Since 2007, the club continued to grow and develop under the leadership of Mandala Buddy. PTK Manila was instrumental in creating pioneering
                        PTK schools in countries such as Turkey and Latvia as well as clubs in Metro Manila and in the rest of the Philippines. Guro Mike Albano,
                        an alumni of PTK Manila, created the first PTK school in Bicol in 2013. Lakan Guro Jay Francisco established the Makati training
                        group in March 2014.
                    </p>
                </div>
            </div>
            <div class="col-md-6 col-md-pull-6 no-space wow fadeInUp" data-wow-duration=".2" data-wow-delay=".1s">
                <div class="padding-20 bg-color-white">
                    <img class="img-responsive" src="http://ptkmanila.com/images/ae56.jpg" alt="">
                </div>
            </div>
        </div>
        <!-- End News v6 -->

        <!-- News v6 -->
        <div class="row padding-lr-20">
            <div class="col-md-6 no-space md-margin-b-30">
                <div class="news-v6 margin-t-50 margin-r-o-50">
                    <div class="margin-b-30">
                        <h3 class="news-v6-title text-uppercase">Reorganization</h3>
                    </div>
                    <p class="font-size-18 margin-b-30 line-height-2">In February 2016, the PTK Manila group left Pekiti Tirsia Kali and reorganized itself as Kali Manila.</p>
                    <p class="font-size-18 margin-b-30 line-height-2">With its long history, experience and dedication to teaching Filipino martial culture, the students and instructors of Kali Manila will continue to improve themselves and do their part in spreading Filipino Martial Arts to new territories around the world.</p>
                </div>
            </div>
            <div class="col-md-6 no-space wow fadeInUp" data-wow-duration=".2" data-wow-delay=".1s">
                <div class="padding-20 bg-color-white">
                    <img class="img-responsive" src="http://ptkmanila.com/images/pg16.jpg" alt="">
                </div>
            </div>
        </div>
        <!-- End News v6 -->
    </div>
</div>
<!-- End News v6 -->

<!-- Call To Action v2 -->
<section class="call-to-action-v2">
    <div class="content-md container">
        <div class="center-content-hor-wrap-sm">
            <div class="center-content-hor-align-sm">
                <h2 class="call-to-action-v2-title">Be part of Kali Manila <br class="hidden-lg hidden-md"> <span class="color-base">(+63) 939 910 6450</span></h2>
                <p class="call-to-action-v2-text">Preserve the culture-rich and highly effective Filipino fighting arts of Kali, as you become a warrior yourself.</p>
            </div>
            <div class="center-content-hor-align-sm text-right">
                <button type="button" class="btn-base-bg btn-base-md radius-3 sidebar-trigger"> <i class="fa fa-envelope-o padding-lr-10"></i> Contact Us</button>
            </div>
        </div>
    </div>
</section>
<!-- End Call To Action v2 -->

@endsection