@extends('layout.master')
@section('content')

    <!-- Blog Teaser -->
    <div class="container margin-b-80">
        <div class="row margin-t-50">
            <div class="col-md-9">
                <div class="content-sm container">
                    <div class="row">
                        <div class="col-md-9 md-margin-b-50">
                            <!-- Blog Grid -->
                            <article class="blog-grid margin-b-30">
                                <!-- Image -->
                                <img class="img-responsive" src="http://placehold.it/800x400" alt="">
                                <!-- End Image -->

                                <!-- Blog Grid Content -->
                                <div class="blog-grid-content">
                                    <h1 class="blog-grid-title-lg text-uppercase"><a class="blog-grid-title-link" href="blog_single_standard.html">The best multimedia experience with Galaxy S6 edge+.</a></h1>

                                    <p class="margin-b-20"><span class="dropcap-dark">L</span>orem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum vel sapien et lacus tempus varius. In finibus lorem vel neque vulputate, vel porta magna molestie. Sed eu egestas ex, a posuere libero. Quisque ac placerat felis. Etiam non neque ac odio consequat interdum vitae eget sem. Interdum et malesuada fames ac ante ipsum primis in faucibus. In et est ut metus lacinia pretium. Donec consequat, ligula eget suscipit laoreet, sem orci molestie tellus, ut pellentesque erat augue non neque.</p>

                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum vel sapien et lacus tempus varius. In finibus lorem vel neque vulputate, vel porta magna molestie. Sed eu egestas ex, a posuere libero. Quisque ac placerat felis. Etiam non neque ac odio consequat <a href="#">interdum vitae</a> eget sem. Interdum et malesuada fames ac ante ipsum primis in faucibus. In et est ut metus lacinia pretium. Donec consequat, ligula eget suscipit laoreet, sem orci molestie tellus, ut pellentesque erat augue non neque. Donec fringilla aliquam dictum. Sed dictum tempus ligula, quis efficitur lacus varius et.</p>

                                    <blockquote class="blockquotes-v4">
                                        <p class="blockquotes-v4-text">Quisque lobortis luctus sodales. Donec placerat turpis quis ultricies tincidunt. Morbi eget pulvinar nibh. Donec lorem ipsum, laoreet sed pellentesque sed, lobortis eget ligula. Maecenas vel magna varius, venenatis lorem et, vehicula lacus.</p>
                                        <span class="blockquotes-v4-by">Adelya Nh.</span>
                                    </blockquote>

                                    <p>Ut pellentesque viverra leo, vel tristique purus efficitur nec. Nam eu nunc in diam vulputate pharetra a non ex. Donec sit amet arcu ultrices, porttitor lorem at, egestas sapien. Proin ut enim metus. In eu augue urna.</p>

                                    <p>Nam bibendum urna in arcu mollis suscipit. Nulla facilisi. Pellentesque finibus arcu ac diam rhoncus, sed pharetra est pellentesque. Duis imperdiet <a href="#">magna ut condimentum ullamcorper</a>.</p>

                                    <p>Ut ultricies, sapien vitae tempor accumsan, tellus nisl maximus nisi, ornare varius urna sapien vestibulum lectus. Phasellus dapibus suscipit leo, fermentum fermentum purus maximus eget. Donec quis facilisis orci. Quisque eleifend sapien in blandit consectetur.</p>

                                    <div class="row margin-t-30 margin-b-30">
                                        <div class="col-sm-8 col-sm-offset-2">
                                            <div id="blog-single-post-standard" class="carousel slide carousel-fade" data-ride="carousel">
                                                <!-- Wrapper for slides -->
                                                <div class="carousel-inner" role="listbox">
                                                    <div class="item active">
                                                        <img class="img-responsive" src="assets/img/970x647/11.jpg" alt="">
                                                    </div>
                                                    <div class="item">
                                                        <img class="img-responsive" src="assets/img/970x647/06.jpg" alt="">
                                                    </div>
                                                </div>
                                                <!-- End Wrapper for slides -->

                                                <!-- Controls -->
                                                <a class="left carousel-control theme-carousel-control-v1" href="#blog-single-post-standard" role="button" data-slide="prev">
                                                    <span class="carousel-control-arrows-v1 radius-3 fa fa-angle-left" aria-hidden="true"></span>
                                                    <span class="sr-only">Previous</span>
                                                </a>
                                                <a class="right carousel-control theme-carousel-control-v1" href="#blog-single-post-standard" role="button" data-slide="next">
                                                    <span class="carousel-control-arrows-v1 radius-3 fa fa-angle-right" aria-hidden="true"></span>
                                                    <span class="sr-only">Next</span>
                                                </a>
                                                <!-- End Controls -->
                                            </div>
                                        </div>
                                    </div>
                                    <!--// end row -->

                                    <p>Donec condimentum luctus tellus, ac eleifend felis imperdiet sed. Donec cursus, tortor vitae lacinia tempus, lacus eros pretium lorem, ut tincidunt nisi mauris ut libero. Praesent in mauris nec est lobortis auctor et vitae turpis. Morbi justo lectus, sollicitudin ac suscipit eu, congue non est. Nam consequat congue justo, non pellentesque urna dapibus vel. Curabitur leo turpis, tempus id tincidunt non, pharetra sed urna. Suspendisse id hendrerit velit.</p>

                                    <!-- List Item Dark -->
                                    <ul class="list-unstyled lists-dark">
                                        <li><i class="lists-item-element fa fa-angle-right"></i> <a href="#">Nam bibendum urna in arcu mollis suscipit. Nulla facilisi</a></li>
                                        <li><i class="lists-item-element fa fa-angle-right"></i> <a href="#">Pellentesque finibus arcu ac diam rhoncus, sed pharetra est pellentesque</a></li>
                                        <li><i class="lists-item-element fa fa-angle-right"></i> <a href="#">Duis imperdiet magna ut condimentum ullamcorper.</a></li>
                                        <li><i class="lists-item-element fa fa-angle-right"></i> <a href="#">Ut ultricies, sapien vitae tempor accumsan, tellus nisl maximus nisi</a></li>
                                        <li><i class="lists-item-element fa fa-angle-right"></i> <a href="#">Phasellus dapibus suscipit leo, fermentum fermentum purus maximus eget</a></li>
                                    </ul>
                                    <!-- End List Item Dark -->

                                    <p>Quisque non tincidunt ante. Vestibulum suscipit eros in mollis sagittis. Nam convallis eros in blandit tempus. Vivamus sodales et ipsum ut varius. Donec ultrices mauris enim, ut sagittis sem bibendum vitae. Mauris vehicula nulla quam, ut scelerisque lorem tincidunt vitae. Quisque viverra dolor vel nibh hendrerit venenatis.</p>

                                    <div class="row margin-t-20 margin-b-20">
                                        <div class="col-xs-6 xs-full-width xs-margin-b-20">
                                            <img class="img-responsive" src="assets/img/970x647/04.jpg" alt="">
                                        </div>
                                        <div class="col-xs-6 xs-full-width">
                                            <img class="img-responsive" src="assets/img/970x647/14.jpg" alt="">
                                        </div>
                                    </div>
                                    <!--// end row -->

                                    <p>In et est ut metus lacinia pretium. Donec consequat, ligula eget suscipit laoreet, sem orci molestie tellus, ut pellentesque erat augue non neque. Donec fringilla aliquam dictum. Sed dictum tempus ligula, quis efficitur lacus varius et. Ut pellentesque viverra leo, vel tristique purus efficitur nec. Nam eu nunc in diam vulputate pharetra a non ex. Donec sit amet arcu ultrices, porttitor lorem at, egestas sapien. Proin ut enim metus.</p>

                                    <blockquote class="blockquotes-v2">Sed tinci dunt odiosedeg semper max lectus</blockquote>

                                    <p>Donec condimentum luctus tellus, ac eleifend felis imperdiet sed. Donec cursus, tortor vitae lacinia tempus, lacus eros pretium lorem, ut tincidunt nisi mauris ut libero. Praesent in mauris nec est lobortis auctor et vitae turpis.</p>

                                    <p>Morbi justo lectus, sollicitudin ac suscipit eu, congue non est. Nam consequat congue justo, non pellentesque urna dapibus vel. Curabitur leo turpis, tempus id tincidunt non, pharetra sed urna. Suspendisse id hendrerit velit.</p>

                                    <p>Mauris dictum, diam vel gravida fringilla, tellus velit vulputate augue, non cursus erat est ac ipsum. Pellentesque porta massa maximus mauris auctor maximus.
                                        Maecenas viverra diam vel sem aliquam rhoncus. Morbi sed lectus eget augue elementum iaculis eu eget erat.</p>

                                    <span class="blog-single-post-source">Source: <a href="#">Prothemes</a></span>

                                    <!-- Blog Grid Tags -->
                                    <ul class="list-inline blog-sidebar-tags">
                                        <li><a class="radius-50" href="#">envato</a></li>
                                        <li><a class="radius-50" href="#">featured</a></li>
                                        <li><a class="radius-50" href="#">wordpress</a></li>
                                    </ul>
                                    <!-- End Blog Grid Tags -->
                                </div>
                                <!-- End Blog Grid Content -->
                            </article>
                            <!-- End Blog Grid -->

                            <!-- Blog Grid -->
                            <div class="bg-color-white margin-b-30">
                                <div class="blog-single-post-content">
                                    <!-- Heading v1 -->
                                    <div class="heading-v1 text-center margin-b-50">
                                        <h2 class="heading-v1-title">You Might Also Like</h2>
                                    </div>
                                    <!-- End Heading v1 -->

                                    <div class="row">
                                        <div class="col-md-4 md-margin-b-30">
                                            <!-- Blog Grid -->
                                            <article class="blog-grid">
                                                <img class="img-responsive" src="assets/img/970x647/19.jpg" alt="">
                                                <div class="blog-grid-content">
                                                    <h2 class="blog-grid-title-sm"><a class="blog-grid-title-link" href="blog_single_standard.html">Architecto beatae vitae dicta sunt explicabo</a></h2>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In finibus lorem vel.</p>
                                                </div>
                                                <div class="blog-grid-supplemental">
                                            <span class="blog-grid-supplemental-title">
                                                <a class="blog-grid-supplemental-category" href="#">News</a>
                                                - 12/21/2016
                                            </span>
                                                </div>
                                            </article>
                                            <!-- End Blog Grid -->
                                        </div>
                                        <div class="col-md-4 md-margin-b-30">
                                            <!-- Blog Grid (Slider) -->
                                            <article class="blog-grid">
                                                <!-- (Slider) -->
                                                <div id="blog-grid-2-col" class="carousel slide carousel-fade" data-ride="carousel">
                                                    <!-- Wrapper for slides -->
                                                    <div class="carousel-inner" role="listbox">
                                                        <div class="item active">
                                                            <img class="img-responsive" src="assets/img/970x647/28.jpg" alt="">
                                                        </div>
                                                        <div class="item">
                                                            <img class="img-responsive" src="assets/img/970x647/17.jpg" alt="">
                                                        </div>
                                                    </div>
                                                    <!-- End Wrapper for slides -->

                                                    <!-- Controls -->
                                                    <a class="left carousel-control theme-carousel-control-v1" href="#blog-grid-2-col" role="button" data-slide="prev">
                                                        <span class="carousel-control-arrows-v1 radius-3 fa fa-angle-left" aria-hidden="true"></span>
                                                        <span class="sr-only">Previous</span>
                                                    </a>
                                                    <a class="right carousel-control theme-carousel-control-v1" href="#blog-grid-2-col" role="button" data-slide="next">
                                                        <span class="carousel-control-arrows-v1 radius-3 fa fa-angle-right" aria-hidden="true"></span>
                                                        <span class="sr-only">Next</span>
                                                    </a>
                                                    <!-- End Controls -->
                                                </div>
                                                <!-- End (Slider) -->

                                                <div class="blog-grid-content">
                                                    <h2 class="blog-grid-title-sm"><a class="blog-grid-title-link" href="blog_single_slider.html">Nemo enim ipsam voluptatem quia</a></h2>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In finibus lorem vel.</p>
                                                </div>
                                                <div class="blog-grid-supplemental">
                                            <span class="blog-grid-supplemental-title">
                                                <a class="blog-grid-supplemental-category" href="#">Opinion</a>
                                                - 12/21/2016
                                            </span>
                                                </div>
                                            </article>
                                            <!-- End Blog Grid Slider -->
                                        </div>
                                        <div class="col-md-4">
                                            <!-- Blog Grid -->
                                            <article class="blog-grid">
                                                <img class="img-responsive" src="assets/img/970x647/18.jpg" alt="">
                                                <div class="blog-grid-content">
                                                    <h2 class="blog-grid-title-sm"><a class="blog-grid-title-link" href="blog_single_standard.html">Sed quia consequuntur magni dolores</a></h2>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In finibus lorem vel.</p>
                                                </div>
                                                <div class="blog-grid-supplemental">
                                            <span class="blog-grid-supplemental-title">
                                                <a class="blog-grid-supplemental-category" href="#">News</a>
                                                - 12/21/2016
                                            </span>
                                                </div>
                                            </article>
                                            <!-- End Blog Grid -->
                                        </div>
                                    </div>
                                    <!--// end row -->
                                </div>
                            </div>
                            <!-- End Blog Grid -->

                            <!-- Blog Comment -->
                            <div class="bg-color-white margin-b-30">
                                <div class="blog-single-post-content">
                                    <!-- Heading v1 -->
                                    <div class="heading-v1 text-center margin-b-50">
                                        <h2 class="heading-v1-title">Leave a comment</h2>
                                    </div>
                                    <!-- End Heading v1 -->

                                    <!-- Single Post Comment Form -->
                                    <div class="blog-single-post-comment-form">
                                        <!-- Comment Form -->
                                        <form id="comment-form" class="comment-form-error" action="index.html" method="get">
                                            <div class="row">
                                                <div class="col-md-4 margin-b-30">
                                                    <input type="text" class="form-control blog-single-post-form radius-3" placeholder="First Name *" name="firstname" required>
                                                </div>
                                                <div class="col-md-4 margin-b-30">
                                                    <input type="text" class="form-control blog-single-post-form radius-3" placeholder="Last Name">
                                                </div>
                                                <div class="col-md-4 margin-b-30">
                                                    <input type="email" class="form-control blog-single-post-form radius-3" placeholder="Email *" name="email" required>
                                                </div>
                                            </div>
                                            <!--// end row -->

                                            <div class="margin-b-30">
                                                <textarea class="form-control blog-single-post-form radius-3" rows="6" placeholder="Your message *" name="textarea" required></textarea>
                                            </div>
                                            <button type="submit" class="btn-dark-brd btn-base-sm footer-v5-btn radius-3">Submit</button>
                                        </form>
                                        <!-- End Comment Form -->

                                        <hr class="md-hr">

                                        <button type="button" class="btn-base btn-base-bg btn-base-md btn-block radius-3 margin-b-30">Show 5 new comments</button>

                                        <!-- Single Post Comment -->
                                        <div class="blog-single-post-comment blog-single-post-comment-first-child">
                                            <div class="blog-single-post-comment-media">
                                                <img class="blog-single-post-comment-media-img radius-circle" src="assets/img/members/04.jpg" alt="">
                                            </div>
                                            <div class="blog-single-post-comment-content">
                                                <h4 class="blog-single-post-comment-username"><a href="#">Katrina Ls.</a></h4>
                                                <small class="blog-single-post-comment-time" title="5 September, 2016">3 hours ago</small>
                                                <p class="blog-single-post-comment-text">Mauris dictum, diam vel gravida fringilla, tellus velit vulputate augue, non cursus erat est ac ipsum. Pellentesque porta massa maximus mauris auctor maximus. Maecenas viverra diam vel sem aliquam rhoncus. Morbi sed lectus eget augue elementum iaculis eu eget erat.</p>

                                                <!-- Single Post Comment Share -->
                                                <ul class="list-inline blog-single-post-comment-share">
                                                    <li class="blog-single-post-comment-share-item">
                                                        <a class="blog-single-post-comment-share-link" href="#">
                                                            <i class="fa fa-thumbs-o-up"></i>
                                                        </a>
                                                    </li>
                                                    <li class="blog-single-post-comment-share-item">
                                                        <a class="blog-single-post-comment-share-link" href="#">
                                                            <i class="fa fa-thumbs-o-down"></i>
                                                        </a>
                                                    </li>
                                                    <li class="blog-single-post-comment-share-item">
                                                        <a class="blog-single-post-comment-share-link" href="#">
                                                            Answer
                                                        </a>
                                                    </li>
                                                    <li class="blog-single-post-comment-share-item">
                                                        <a class="blog-single-post-comment-share-link" href="#">
                                                            Share
                                                        </a>
                                                    </li>
                                                    <li class="blog-single-post-comment-share-item pull-right">
                                                        <a class="blog-single-post-comment-share-link" href="#">
                                                            Show 3 new answers
                                                        </a>
                                                    </li>
                                                </ul>
                                                <!-- End Single Post Comment Share -->
                                            </div>

                                            <!-- Single Post Comment -->
                                            <div class="blog-single-post-comment">
                                                <div class="blog-single-post-comment-media">
                                                    <img class="blog-single-post-comment-media-img radius-circle" src="assets/img/members/02.jpg" alt="">
                                                </div>
                                                <div class="blog-single-post-comment-content">
                                                    <h4 class="blog-single-post-comment-username"><a href="#">Sara Glaser</a></h4>
                                                    <small class="blog-single-post-comment-time" title="5 September, 2016">2 hours ago</small>
                                                    <p class="blog-single-post-comment-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum vel sapien et lacus tempus varius.</p>

                                                    <!-- Single Post Comment Share -->
                                                    <ul class="list-inline blog-single-post-comment-share">
                                                        <li class="blog-single-post-comment-share-item">
                                                            <a class="blog-single-post-comment-share-link" href="#">
                                                                <i class="fa fa-thumbs-o-up"></i>
                                                            </a>
                                                        </li>
                                                        <li class="blog-single-post-comment-share-item">
                                                            <a class="blog-single-post-comment-share-link" href="#">
                                                                <i class="fa fa-thumbs-o-down"></i>
                                                            </a>
                                                        </li>
                                                        <li class="blog-single-post-comment-share-item">
                                                            <a class="blog-single-post-comment-share-link" href="#">
                                                                Answer
                                                            </a>
                                                        </li>
                                                        <li class="blog-single-post-comment-share-item">
                                                            <a class="blog-single-post-comment-share-link" href="#">
                                                                Share
                                                            </a>
                                                        </li>
                                                    </ul>
                                                    <!-- End Single Post Comment Share -->
                                                </div>
                                            </div>
                                            <!-- End Single Post Comment -->
                                        </div>
                                        <!-- End Blog Single Post Comment -->

                                        <!-- Single Post Comment -->
                                        <div class="blog-single-post-comment">
                                            <div class="blog-single-post-comment-media">
                                                <img class="blog-single-post-comment-media-img radius-circle" src="assets/img/members/03.jpg" alt="">
                                            </div>
                                            <div class="blog-single-post-comment-content">
                                                <h4 class="blog-single-post-comment-username"><a href="#">Robert Smith</a></h4>
                                                <small class="blog-single-post-comment-time" title="5 September, 2016">5 hours ago</small>
                                                <p class="blog-single-post-comment-text">Interdum nibh vel varius quam velit at massa. Nunc in metus sagittis, dictum dui a, feugiat tortor. Aenean sem augue, vestibulum sagittis commodo sed, mollis vel nunc.</p>

                                                <!-- Single Post Comment Share -->
                                                <ul class="list-inline blog-single-post-comment-share">
                                                    <li class="blog-single-post-comment-share-item">
                                                        <a class="blog-single-post-comment-share-link" href="#">
                                                            <i class="fa fa-thumbs-o-up"></i>
                                                        </a>
                                                    </li>
                                                    <li class="blog-single-post-comment-share-item">
                                                        <a class="blog-single-post-comment-share-link" href="#">
                                                            <i class="fa fa-thumbs-o-down"></i>
                                                        </a>
                                                    </li>
                                                    <li class="blog-single-post-comment-share-item">
                                                        <a class="blog-single-post-comment-share-link" href="#">
                                                            Answer
                                                        </a>
                                                    </li>
                                                    <li class="blog-single-post-comment-share-item">
                                                        <a class="blog-single-post-comment-share-link" href="#">
                                                            Share
                                                        </a>
                                                    </li>
                                                </ul>
                                                <!-- End Single Post Comment Share -->
                                            </div>
                                        </div>
                                        <!-- End Single Post Comment -->
                                    </div>
                                    <!-- Single Post Comment Form -->
                                </div>
                            </div>
                            <!-- End Blog Comment -->
                        </div>

                    </div>
                    <!--// end row -->
                </div>
            </div>
            <!--========== BLOG SIDEBAR ==========-->
            <div class="col-md-3">
                @include('layout.widgets.blog-sidebar')
            </div>
            <!--========== END BLOG SIDEBAR ==========-->
        </div>
    </div>
    <!-- End Blog Teaser -->

@endsection