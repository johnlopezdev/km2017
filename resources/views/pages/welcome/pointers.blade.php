<div class="content-lg container">
    <div class="row">
        <div class="col-sm-4 sm">
            <div class="heading-v3 text-center">
                <p class="">
                    <i class="pe-7s-add-user fa-3x"></i>
                </p>
                <h3>Acquire self-defense skills most effective in real-life situations.</h3>
                <br class="visible-xs visible-sm">
            </div>
        </div>
        <div class="col-sm-4">
            <div class="heading-v3 text-center">
                <p class="">
                    <i class="pe-7s-target fa-3x"></i>
                </p>
                <h3>We focus on training. No politics. Just training.</h3>
                <br class="visible-xs visible-sm">
            </div>
        </div>
        <div class="col-sm-4">
            <div class="heading-v3 text-center">
                <p class="">
                    <i class="pe-7s-gleam fa-3x"></i>
                </p>
                <h3>Violence has no rules. Kali is no sport. Learn how to survive.</h3>
                <br class="visible-xs visible-sm">
            </div>
        </div>
    </div>
</div>