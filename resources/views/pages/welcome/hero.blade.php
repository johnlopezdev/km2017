<div id="rev_slider_20_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="image-hero20" style="margin:0px auto;background-color:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
    <!-- START REVOLUTION SLIDER 5.0.7 fullwidth mode -->
    <div id="rev_slider_20_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.0.7">
        <ul>
            <!-- SLIDE  -->
            <li data-index="rs-68" data-transition="zoomout" data-slotamount="default"  data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000"  data-thumb="assets/img/content-img/notgenericherobg-100x50.jpg"  data-rotate="0"  data-saveperformance="off"  data-title="Intro" data-description="">
                <!-- MAIN IMAGE -->
                <img src="http://ptkmanila.com/images/pg1.jpg"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                <!-- LAYERS -->
                <!-- LAYER NR. 1 -->
                <div class="tp-caption tp-shape tp-shapewrapper   rs-parallaxlevel-0"
                     id="slide-68-layer-10"
                     data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                     data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
                     data-width="full"
                     data-height="full"
                     data-whitespace="nowrap"
                     data-transform_idle="o:1;"
                     data-transform_in="opacity:0;s:1500;e:Power3.easeInOut;"
                     data-transform_out="s:300;s:300;"
                     data-start="750"
                     data-basealign="slide"
                     data-responsive_offset="on"
                     data-responsive="off"
                     style="z-index: 5;background-color:rgba(0, 0, 0, 0.40);border-color:rgba(0, 0, 0, 0.50);">
                </div>
                <!-- LAYER NR. 3 -->
                <div class="tp-caption NotGeneric-Title   tp-resizeme rs-parallaxlevel-0"
                     id="slide-68-layer-1"
                     data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                     data-y="['middle','middle','middle','middle']" data-voffset="['0','0','-22','-29']"
                     data-fontsize="['70','70','70','50']"
                     data-lineheight="['70','70','70','50']"
                     data-width="none"
                     data-height="none"
                     data-whitespace="nowrap"
                     data-transform_idle="o:1;"
                     data-transform_in="z:0;rX:0deg;rY:0;rZ:0;sX:1.5;sY:1.5;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;"
                     data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                     data-mask_in="x:0px;y:0px;"
                     data-mask_out="x:inherit;y:inherit;"
                     data-start="1000"
                     data-splitin="none"
                     data-splitout="none"
                     data-responsive_offset="on"
                     style="z-index: 7; white-space: nowrap;">KALI MANILA
                </div>
                <!-- LAYER NR. 4 -->
                <div class="tp-caption NotGeneric-SubTitle   tp-resizeme rs-parallaxlevel-0"
                     id="slide-68-layer-4"
                     data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                     data-y="['middle','middle','middle','middle']" data-voffset="['52','52','28','13']"
                     data-width="none"
                     data-height="none"
                     data-whitespace="nowrap"
                     data-transform_idle="o:1;"
                     data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:1500;e:Power4.easeInOut;"
                     data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                     data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;"
                     data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                     data-start="1000"
                     data-splitin="none"
                     data-splitout="none"
                     data-responsive_offset="on"
                     style="z-index: 8; white-space: nowrap; font-size: 1vw"
                >
                    <div class="white text-uppercase text-center">
                        Edged, Impact and Empty Hand <br class="hidden-lg hidden-md"> Combat Arts of Filipinos
                    </div>
                </div>

                <!--Layer 5-->
                <div class="tp-caption NotGeneric-CallToAction rev-btn  rs-parallaxlevel-0"
                     id="slide-68-layer-7"
                     data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                     data-y="['middle','middle','middle','middle']" data-voffset="['124','124','80','65']"
                     data-width="none"
                     data-height="none"
                     data-whitespace="nowrap"
                     data-transform_idle="o:1;"
                     data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power1.easeInOut;"
                     data-style_hover="c:rgba(255, 255, 255, 1.00);bc:rgba(255, 255, 255, 1.00);cursor:pointer;"
                     data-transform_in="y:50px;opacity:0;s:1500;e:Power4.easeInOut;"
                     data-transform_out="y:[175%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                     data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                     data-start="1250"
                     data-splitin="none"
                     data-splitout="none"
                     data-actions='[{"event":"click","action":"scrollbelow","offset":"0px"}]'
                     data-responsive_offset="on"
                     data-responsive="off"
                     style="z-index: 9; white-space: nowrap;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;"
                >
                    <span class="text-uppercase hidden-sm hidden-xs"><i class="pe-7s-news-paper"></i> Mandala Buddy's interview on MMA Philippines</span>
                    <span class="text-uppercase hidden-lg hidden-md"><i class="pe-7s-news-paper"></i> Latest News</span>
                </div>
            </li>
        </ul>
        <div class="tp-static-layers"></div>
        <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
    </div>
</div>