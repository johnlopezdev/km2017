<div class="full-width-container">
    <div class="row no-space-row">
        <div class="col-md-6">
            <!-- Blog Teaser -->
            <article class="blog-teaser">
                <img src="http://ptkmanila.com/images/ae59.jpg" width="970" height="580" alt="">
                <div class="blog-teaser-overlay">
                    <div class="blog-teaser-center-align">
                        <h2 class="blog-teaser-title">Training</h2>
                        <div class="blog-teaser-text">
                            <h3>Train to survive the worst situations. Learn to pick up weapons of opportunity and discover how weapons training leads to empty-handed self-defense skills.</h3>
                        </div>
                    </div>
                </div>
                {{--<a class="blog-teaser-link" href="#"></a>--}}
            </article>
            <!-- End Blog Teaser -->
        </div>
        <div class="col-md-6">
            <!-- Blog Teaser -->
            <article class="blog-teaser">
                <img src="http://ptkmanila.com/images/i21.jpg" width="970" height="580" alt="">
                <div class="blog-teaser-overlay bg-danger">
                    <div class="blog-teaser-center-align">
                        <h2 class="blog-teaser-title">Brotherhood</h2>
                        <div class="blog-teaser-text">
                            <h3>More training, less politics. Kali Manila is a community of dedicated lifelong students of the Filipino Martial Arts.</h3>
                        </div>
                    </div>
                </div>
                {{--<a class="blog-teaser-link" href="#"></a>--}}
            </article>
            <!-- End Blog Teaser -->
        </div>
    </div>
    <!--// end row -->

    <div class="row no-space-row">
        <div class="col-md-6">
            <!-- Blog Teaser -->
            <article class="blog-teaser">
                <img src="http://ptkmanila.com/images/b3.jpg" width="970" height="580" alt="">
                <div class="blog-teaser-overlay">
                    <div class="blog-teaser-center-align">
                        <h2 class="blog-teaser-title">Experience</h2>
                        <div class="blog-teaser-text">
                            <h3>Kali Manila is a new name for a group that has been in existence since 2002. It had produced exceptional students and instructors both in the Philippines and in other countries.</h3>
                        </div>
                    </div>
                </div>
                {{--<a class="blog-teaser-link" href="#"></a>--}}
            </article>
            <!-- End Blog Teaser -->
        </div>
        <div class="col-md-6">
            <!-- Blog Teaser -->
            <article class="blog-teaser">
                <img src="http://ptkmanila.com/images/ap180.jpg" width="970" height="580" alt="">
                <div class="blog-teaser-overlay">
                    <div class="blog-teaser-center-align">
                        <h2 class="blog-teaser-title">Culture</h2>
                        <div class="blog-teaser-text">
                            <h3>The Filipino Martial Arts are considered one of the most effective fighting systems in the world. Known for its devastating and creative use of weapons, the systems have been adopted by military and police units in the Philippines and abroad. </h3>
                        </div>
                    </div>
                </div>
                {{--<a class="blog-teaser-link" href="#"></a>--}}
            </article>
            <!-- End Blog Teaser -->
        </div>
    </div>
    <!--// end row -->
</div>