@extends('layout.master')
@section('content')
<!-- FAQ Content -->
<div class="bg-color-sky-light">
    <div class="content-md container">
        <div class="row margin-b-50">
            <div class="col-sm-12">
                <div class="margin-b-20">
                    <div class="margin-b-30">
                        <h2 class=""><i class="pe-7s-users fa-4x margin-r-20"></i> <br> EXPECTATIONS IN TRAINING</h2>
                        <blockquote>
                            <p class="font-size-20 margin-b-20">
                                <span class="text-capitalize h4">KALI IS NO SPORT</span><br>
                                Most of our students are referred to us by fellow students and those we know. Due to the nature of our edged weapons training, the teaching staff will interview all prospective students prior to accepting them for training.
                            </p>
                            <p class="font-size-20 margin-b-20">
                                While many martial art styles are well-known in sports competitions, allow us to emphasize that Kali is NOT one of them.
                                The fighting techniques taught are dangerous enough that prospective students must be at least 18 years old. As such, the club also reserves the right to refuse prospective students for various reasons.
                            </p>
                        </blockquote>
                        <blockquote>
                            <p class="font-size-20 margin-b-20">
                                <span class="text-capitalize h4">PHYSICAL FITNESS</span><br>
                                We may require new students to present a medical certificate attesting to the student's fitness to do strenuous activity. The teaching staff must be informed of any chronic injury that may affect training. Such limitations are not necessarily grounds for being refused membership in the group.
                            </p>
                        </blockquote>
                        <blockquote>
                            <p class="font-size-20 margin-b-20">
                                <span class="text-capitalize h4">PROPER ATTIRE AND EQUIPMENT</span><br>
                                Beginners are required to train in exercise clothes and to bring their own drinking water. We don't require or recommend that beginners buy their own training weapons right away. We can supply sticks and other training weapons for the first classes. Beginners can later purchase these from our club's suppliers.
                            </p>
                        </blockquote>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <!-- Training Info -->
            <div class="col-md-4">
                <div class="border-bottom-1 margin-b-30">
                    <div class="margin-b-20">
                        <div class="margin-b-30">
                            <h2 class=""><i class="pe-7s-timer fa-4x margin-r-20"></i> <br> SCHEDULE &amp; VENUE</h2>
                            <p class="font-size-20 margin-b-20">Classes are held every Monday and Wednesday, 8pm to 10pm, at the basketball court of Blue Ridge B, Blue Ridge, Quezon City. The community is at the corner of Santolan and Katipunan Avenue. Please refer to our maps for more details.</p>
                            <p class="font-size-20 margin-b-20">There is a Makati class being conducted by Lakan Guro Jay Francisco</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="border-bottom-1 margin-b-30">
                    <div class="margin-b-20">
                        <div class="margin-b-30">
                            <h2 class=""><i class="pe-7s-target fa-4x margin-r-20"></i> <br> CURRICULUM</h2>
                            <p class="font-size-20 margin-b-20">Kali Manila teaches a form of Filipino Martial Arts which includes:</p>
                            <div class="font-size-20 margin-b-20">
                                <ul class="line-height-2">
                                    <li>Single stick and double sticks</li>
                                    <li>Single sword and double sword</li>
                                    <li>Single knife and double knives</li>
                                    <li>Sword and dagger</li>
                                    <li>Utilizing random objects as weapons</li>
                                    <li>Unarmed or empty-hand techniques</li>
                                    <li>Joint manipulation and grappling</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="border-bottom-1 margin-b-30">
                    <div class="margin-b-20">
                        <div class="margin-b-30">
                            <h2 class=""><i class="pe-7s-date fa-4x margin-r-20"></i> <br> TRAINING FEE</h2>
                            <p class="font-size-20 margin-b-20">Kali Manila charges a monthly training fee. We do not have an annual membership fee or daily rate. Please email us for details on our current rate.</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Training Info -->
        </div>
    </div>
</div>
<!-- End FAQ Content -->
@endsection
