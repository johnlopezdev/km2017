<div class="form-modal">
    <div class="form-modal-wrap">
        <div class="form-modal-container">
            <!-- Login Form -->
            <div id="form-modal-login">
                <div class="form-modal-heading">
                    <div class="text-center">
                        <img src="{{ asset('assets/img/250x250/kali-icon-white.png') }}" width="50" alt="">
                    </div>
                    <h2 class="form-modal-title">Login</h2>
                    <p>Use Facebook or an email</p>
                </div>
                <div class="clearfix margin-b-20">
                    <div class="form-modal-connect">
                        <button type="button" class="btn-fb-bg btn-base-animate-to-top btn-base-sm btn-block radius-3">Facebook
                            <span class="btn-base-element-sm radius-3"><i class="btn-base-element-icon fa fa-facebook"></i></span>
                        </button>
                    </div>
                    <div class="form-modal-divider">
                        <span class="form-modal-divider-text">or</span>
                    </div>
                    <form class="form-modal-login-form">
                        <fieldset>
                            <input class="form-control form-modal-input radius-3 margin-b-10" id="signin-email" type="email" placeholder="Email Address">
                        </fieldset>
                        <fieldset class="form-modal-input-group">
                            <input class="form-control form-modal-input form-modal-input-form radius-3 margin-b-10" id="signin-password" type="text" placeholder="Password">
                            <a class="form-modal-hide-password" href="javascript:void(0);">Hide</a>
                        </fieldset>
                        <fieldset>
                            <button type="submit" class="btn-base-bg btn-base-sm btn-block radius-3">Login</button>
                        </fieldset>
                    </form>
                </div>
                <p class="form-modal-back-btn-message">Forgot your password? <a class="form-modal-back-btn" href="javascript:void(0);">Reset</a></p>
                <div class="form-modal-switcher text-center">
                    <div class="form-modal-switcher-item form-modal-back-btn-message">
                        Join Kali Manila
                        <a class="form-modal-back-btn-message-link" href="javascript:void(0);">Signup</a>
                    </div>
                </div>
            </div>
            <!-- End Login Form -->

            <!-- Signup -->
            <div id="form-modal-signup">
                <div class="form-modal-heading">
                    <h2 class="form-modal-title">Signup for free</h2>
                    <p class="form-modal-paragraph">Use Facebook or an email</p>
                </div>
                <div class="clearfix margin-b-20">
                    <div class="form-modal-connect">
                        <button type="button" class="btn-fb-bg btn-base-animate-to-top btn-base-sm btn-block radius-3">Facebook
                            <span class="btn-base-element-sm radius-3"><i class="btn-base-element-icon fa fa-facebook"></i></span>
                        </button>
                    </div>
                    <div class="form-modal-divider">
                        <span class="form-modal-divider-text">or</span>
                    </div>
                    <form class="form-modal-login-form margin-b-20">
                        <fieldset>
                            <input class="form-control form-modal-input radius-3 margin-b-10" id="signup-username" type="text" placeholder="Username">
                        </fieldset>
                        <fieldset>
                            <input class="form-control form-modal-input radius-3 margin-b-10" id="signup-email" type="email" placeholder="Email Address">
                        </fieldset>
                        <fieldset class="form-modal-input-group">
                            <input class="form-control form-modal-input form-modal-input-form radius-3 margin-b-10" id="signup-password" type="text" placeholder="Password">
                            <a class="form-modal-hide-password" href="javascript:void(0);">Hide</a>
                        </fieldset>
                        <fieldset>
                            <button type="submit" class="btn-base-bg btn-base-sm btn-block radius-3">Create Account</button>
                        </fieldset>
                    </form>
                    <div class="form-modal-back-btn-message">
                        By signing up you agree to the
                        <a href="#">Terms &amp; Conditions</a>
                        and
                        <a href="#">Privacy &amp; Policy</a>
                    </div>
                </div>
            </div>
            <!-- End Signup -->

            <!-- Reset Password -->
            <div id="form-modal-reset-password">
                <div class="form-modal-heading">
                    <h2 class="form-modal-title">Reset password</h2>
                    <p class="form-modal-paragraph">Enter email to reset password</p>
                </div>
                <div class="clearfix margin-b-20">
                    <form class="form-modal-reset-form">
                        <fieldset>
                            <input class="form-control form-modal-input radius-3 margin-b-10" id="reset-email" type="email" placeholder="Email Address">
                        </fieldset>
                        <fieldset class="margin-b-20">
                            <button type="submit" class="btn-base-bg btn-base-sm btn-block radius-3">Reset</button>
                        </fieldset>
                        <div class="form-modal-back-btn-message">
                            Already have account?
                            <a class="form-modal-back-btn form-modal-back-btn-message-link" href="javascript:void(0);">Login</a>
                        </div>
                        <div class="form-modal-switcher">
                            <div class="form-modal-switcher-item form-modal-back-btn-message">
                                Join Kali Manila
                                <a class="form-modal-back-btn-message-link" href="javascript:void(0);">Signup</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- End Reset Password -->
        </div>
    </div>
    <a href="javascript:void(0);" class="form-modal-close-form">&times;</a>
</div>