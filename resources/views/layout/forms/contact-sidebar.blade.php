<div class="sidebar-nav scrollbar">
    <a class="sidebar-trigger sidebar-nav-trigger" href="javascript:void(0);">
        <span class="sidebar-trigger-icon"></span>
    </a>
    <div class="sidebar-nav-content">
        <h3 class="sidebar-nav-title">Contact Us</h3>
        <p class="sidebar-nav-about-text"><i class="fa fa-envelope"></i>&nbsp;&nbsp; ptkmanila123-az@gmail.com</p>
        <p class="sidebar-nav-about-text"><i class="fa fa-phone"></i>&nbsp;&nbsp; +63 939 910 6450</p>
        <p class="sidebar-nav-about-text"><i class="fa fa-facebook"></i>&nbsp;&nbsp; <a href="https://www.facebook.com/groups/209300225757766/">Kali Manila</a></p>
        <p class="sidebar-nav-about-text"><i class="fa fa-map-marker"></i>&nbsp;&nbsp; <a href="https://www.google.com/maps/@14.617959,121.07411,17z">Google Maps</a></p>
        <p class="sidebar-nav-about-text"><i class="fa fa-twitter"></i>&nbsp;&nbsp; <a href="https://twitter.com/PTK_Manila">@PTK_Manila</a></p>

        <hr>

        <input type="text" class="form-control sidebar-nav-comment-input margin-b-10 radius-3" placeholder="Your Name">
        <input type="email" class="form-control sidebar-nav-comment-input margin-b-10 radius-3" placeholder="Your e-Mail">
        <textarea class="form-control sidebar-nav-comment-input margin-b-30 radius-3" rows="3" placeholder="Your message"></textarea>
        <button type="button" class="btn-white-brd btn-base-sm radius-3">Submit</button>
    </div>
</div>