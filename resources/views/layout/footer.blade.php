<footer id="footer" class="footer">
    <div class="container">

        <!-- Copyright -->
        <ul class="list-inline footer-copyright">
            <li class="footer-copyright-item">Copyright &#169; 2016 Kali Manila. All Rights Reserved.</li>
            <li class="footer-copyright-item"><a href="#">Privacy &amp; Policy</a></li>
            <li class="footer-copyright-item-toggle-trigger"><a class="footer-toggle-trigger footer-toggle-trigger-style" href="javascript:void(0);">View Branches</a></li>
        </ul>
        <!-- End Copyright -->

        <!-- Collapse -->
        <div class="footer-toggle-collapse footer-toggle">
            <div class="row">
                <div class="col-sm-4 sm-margin-b-50">
                    <h4 class="footer-toggle-title">United States</h4>
                    <p class="footer-toggle-text">1600 Amphitheatre Parkway, Mountain View, CA 94043</p>
                    <p class="footer-toggle-text">Phone: +1 650-253-0000</p>
                    <a class="footer-toggle-link" href="#">Email: us.prothemes.net@gmail.com</a>
                </div>
                <div class="col-sm-4 sm-margin-b-50">
                    <h4 class="footer-toggle-title">Canada</h4>
                    <p class="footer-toggle-text">1253 McGill College Montreal, Quebec, H3B 2Y5</p>
                    <p class="footer-toggle-text">Phone: +1 514-670-8700</p>
                    <a class="footer-toggle-link" href="#">Email: canada.prothemes.net@gmail.com</a>
                </div>
                <div class="col-sm-4">
                    <h4 class="footer-toggle-title">Australia</h4>
                    <p class="footer-toggle-text">Level 5, 48 Pirrama Road, Pyrmont, NSW 2009</p>
                    <p class="footer-toggle-text">Phone: +61 2 9374 4000</p>
                    <a class="footer-toggle-link" href="#">Email: australia.prothemes.net@gmail.com</a>
                </div>
            </div>
            <!--// end row -->
        </div>
        <!-- End Collapse -->
    </div>
</footer>