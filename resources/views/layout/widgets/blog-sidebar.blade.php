@include('layout.widgets.blog-sidebar-recent')

<!-- Blog Sidebar -->
<div class="blog-sidebar margin-b-30">
    <div class="blog-sidebar-heading">
        <i class="blog-sidebar-heading-icon icon-chat"></i>
        <h4 class="blog-sidebar-heading-title">Twitter feed</h4>
    </div>
    <div class="blog-sidebar-content blog-sidebar-content-height scrollbar">
        <!-- Twitter Feed -->
        <ul class="list-unstyled twitter-feed">
            <li class="twitter-feed-item">
                <div class="twitter-feed-media">
                    <img class="twitter-feed-media-img radius-circle" src="assets/img/250x250/01.jpg" alt="">
                </div>
                <div class="twitter-feed-content">
                    <strong class="twitter-feed-profile-name">Dr.Cafee</strong>
                    <span class="twitter-feed-profile-nickname"><a class="twitter-feed-profile-nickname-link" href="#">@DrCafee</a></span>
                    <span class="twitter-feed-posted-time">4h</span>
                    <p class="twitter-feed-paragraph">Sequat ultrices metus et malesuada.</p>
                    <a class="twitter-feed-link" href="#">http://bit.ly/1c0UN3Y</a>
                </div>
            </li>
            <li class="twitter-feed-item">
                <div class="twitter-feed-media">
                    <img class="twitter-feed-media-img radius-circle" src="assets/img/250x250/04.jpg" alt="">
                </div>
                <div class="twitter-feed-content">
                    <strong class="twitter-feed-profile-name">Nickos</strong>
                    <span class="twitter-feed-profile-nickname"><a class="twitter-feed-profile-nickname-link" href="#">@Nicko</a></span>
                    <span class="twitter-feed-posted-time">5h</span>
                    <p class="twitter-feed-paragraph">Nam bibendum urna in arcu mollis suscipit.</p>
                    <a class="twitter-feed-link" href="#">http://bit.ly/1c0UN3Y</a>
                </div>
            </li>
            <li class="twitter-feed-item">
                <div class="twitter-feed-media">
                    <img class="twitter-feed-media-img radius-circle" src="assets/img/250x250/02.jpg" alt="">
                </div>
                <div class="twitter-feed-content">
                    <strong class="twitter-feed-profile-name">PhotoStudio</strong>
                    <span class="twitter-feed-profile-nickname"><a class="twitter-feed-profile-nickname-link" href="#">@PS</a></span>
                    <span class="twitter-feed-posted-time">7h</span>
                    <p class="twitter-feed-paragraph">Curabitur leo turpis, tempus id tincidunt non, pharetra sed urna.</p>
                    <a class="twitter-feed-link" href="#">http://bit.ly/1c0UN3Y</a>
                </div>
            </li>
            <li class="twitter-feed-item">
                <div class="twitter-feed-media">
                    <img class="twitter-feed-media-img radius-circle" src="assets/img/250x250/03.jpg" alt="">
                </div>
                <div class="twitter-feed-content">
                    <strong class="twitter-feed-profile-name">Mr.Dog</strong>
                    <span class="twitter-feed-profile-nickname"><a class="twitter-feed-profile-nickname-link" href="#">@Mr.Dog</a></span>
                    <span class="twitter-feed-posted-time">1d</span>
                    <p class="twitter-feed-paragraph">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    <a class="twitter-feed-link" href="#">http://bit.ly/1c0UN3Y</a>
                </div>
            </li>
        </ul>
        <!-- End Twitter Feed -->
    </div>
</div>
<!-- End Blog Sidebar -->

<!-- Blog Sidebar -->
<div class="blog-sidebar">
    <div class="blog-sidebar-heading">
        <i class="blog-sidebar-heading-icon icon-paperclip"></i>
        <h4 class="blog-sidebar-heading-title">Tags</h4>
    </div>
    <div class="blog-sidebar-content">
        <!-- Blog Grid Tags -->
        <ul class="list-inline blog-sidebar-tags">
            <li><a class="radius-50" href="#">envato</a></li>
            <li><a class="radius-50" href="#">featured</a></li>
            <li><a class="radius-50" href="#">material</a></li>
            <li><a class="radius-50" href="#">fashion</a></li>
            <li><a class="radius-50" href="#">themeforest</a></li>
            <li><a class="radius-50" href="#">css3</a></li>
            <li><a class="radius-50" href="#">photoshop</a></li>
            <li><a class="radius-50" href="#">wordpress</a></li>
        </ul>
        <!-- End Blog Grid Tags -->
    </div>
</div>
<!-- End Blog Sidebar -->