<!-- Blog Sidebar -->
<div class="blog-sidebar margin-b-30">
    <div class="blog-sidebar-heading">
        <i class="blog-sidebar-heading-icon icon-book-open"></i>
        <h4 class="blog-sidebar-heading-title">Recent Articles</h4>
    </div>
    <div class="blog-sidebar-content blog-sidebar-content-height scrollbar">
        <!-- Latest Tutorials -->
        <article class="latest-tuts">
            <div class="latest-tuts-media">
                <img class="latest-tuts-media-img radius-circle" src="http://placehold.it/50x50" alt="">
            </div>
            <div class="latest-tuts-content">
                <h5 class="latest-tuts-content-title"><a href="#">Visual brand designing</a></h5>
                <small class="latest-tuts-content-time">35 minutes ago</small>
            </div>
        </article>
        <article class="latest-tuts">
            <div class="latest-tuts-media">
                <img class="latest-tuts-media-img radius-circle" src="http://placehold.it/50x50" alt="">
            </div>
            <div class="latest-tuts-content">
                <h5 class="latest-tuts-content-title"><a href="#">Photoshop: Image Cropping</a></h5>
                <small class="latest-tuts-content-time">7 hours ago</small>
            </div>
        </article>
        <article class="latest-tuts">
            <div class="latest-tuts-media">
                <img class="latest-tuts-media-img radius-circle" src="http://placehold.it/50x50" alt="">
            </div>
            <div class="latest-tuts-content">
                <h5 class="latest-tuts-content-title"><a href="#">Video editing</a></h5>
                <small class="latest-tuts-content-time">12 hours ago</small>
            </div>
        </article>
        <article class="latest-tuts">
            <div class="latest-tuts-media">
                <img class="latest-tuts-media-img radius-circle" src="http://placehold.it/50x50" alt="">
            </div>
            <div class="latest-tuts-content">
                <h5 class="latest-tuts-content-title"><a href="#">Web development technologies</a></h5>
                <small class="latest-tuts-content-time">1 day ago</small>
            </div>
        </article>
        <article class="latest-tuts">
            <div class="latest-tuts-media">
                <img class="latest-tuts-media-img radius-circle" src="http://placehold.it/50x50" alt="">
            </div>
            <div class="latest-tuts-content">
                <h5 class="latest-tuts-content-title"><a href="#">The section element - HTML</a></h5>
                <small class="latest-tuts-content-time">2 days ago</small>
            </div>
        </article>
        <article class="latest-tuts">
            <div class="latest-tuts-media">
                <img class="latest-tuts-media-img radius-circle" src="http://placehold.it/50x50" alt="">
            </div>
            <div class="latest-tuts-content">
                <h5 class="latest-tuts-content-title"><a href="#">Creata a logo using Adobe Illustrator</a></h5>
                <small class="latest-tuts-content-time">3 days ago</small>
            </div>
        </article>
        <!-- End Latest Tutorials -->
    </div>
</div>
<!-- End Blog Sidebar -->