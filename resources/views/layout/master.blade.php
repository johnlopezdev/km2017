<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- HEAD -->
<head>
    @include('layout.master-meta')
    @include('layout.styles')

    <!--jQuery init and other plugins-->
    @include('layout.scripts')

    <!--favicon-->
    @include('layout.favicon')
</head>
<!-- END HEAD -->
<!-- BODY -->
<body class="dark-theme">

<!-- WRAPPER -->
<div class="wrapper animsition wrapper-top-space">
    <!--========== HEADER ==========-->
    @include('layout.header')
    <!--========== END HEADER ==========-->

    <!--========== FORM MODAL ==========-->
    @include('layout.forms.login-signup')
    <!--========== END FORM MODAL ==========-->

    <!--========== SIDEBAR ==========-->
    @include('layout.forms.contact-sidebar')
    <!--========== END SIDEBAR ==========-->

    <!--========== PAGE CONTENT ==========-->
    @yield('content')
    <!--========== END PAGE CONTENT ==========-->

    <!--========== FOOTER ==========-->
    @include('layout.footer')
    <!--========== END FOOTER ==========-->
</div>
<!-- END WRAPPER -->

<!-- Sidebar Content Overlay -->
<div class="sidebar-content-overlay"></div>
<!-- End Sidebar Content Overlay -->

<!-- Back To Top -->
<a href="javascript:void(0);" class="back-to-top"></a>
<!-- End Back To Top -->

<!--========== JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) ==========-->
@include('layout.scripts.corejs')
<!--========== END JAVASCRIPTS ==========-->
</body>
<!-- END BODY -->

<script>
//    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
//                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
//            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
//    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
//
//    ga('create', 'UA-74599804-1', 'auto');
//    ga('send', 'pageview');

</script>

</html>
