<!-- CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="{{ asset('assets/plugins/html5shiv.js')  }}"></script>
<script src="{{ asset('assets/plugins/respond.min.js') }}"></script>
<![endif]-->
<script type="text/javascript" src="{{ asset('assets/plugins/jquery.migrate.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
<!-- END CORE PLUGINS -->

<!-- PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="{{ asset('assets/plugins/jquery.back-to-top.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/plugins/jquery.smooth-scroll.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/plugins/jquery.animsition.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/plugins/scrollbar/jquery.mCustomScrollbar.concat.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/plugins/counter/waypoints.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/plugins/counter/jquery.counterup.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/plugins/magnific-popup/jquery.magnific-popup.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js') }}"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
<script type="text/javascript" src="{{ asset('assets/plugins/google-map.js') }}"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- PAGE LEVEL SCRIPTS -->
<script type="text/javascript" src="{{ asset('assets/scripts/components/google-map.js') }}"></script>
<!-- END PAGE LEVEL SCRIPTS -->

<!-- PAGE LEVEL SCRIPTS -->
<script type="text/javascript" src="{{ asset('assets/scripts/app.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/scripts/components/animsition.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/scripts/components/scrollbar.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/scripts/components/form-modal.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/scripts/components/counters.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/scripts/components/magnific-popup.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/scripts/portfolio/portfolio-3-col-grid-v2.js') }}"></script>
<!-- END PAGE LEVEL SCRIPTS -->

<!-- REVOLUTION SCRIPT CODE -->
@include('layout.scripts.main-slider-settings')
        <!-- END REVOLUTION SCRIPT CODE -->