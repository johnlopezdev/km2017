<header class="header navbar-fixed-top">
    <!-- Search Field -->
    <div class="search-field">
        <div class="container">
            <input type="text" class="form-control search-field-input" placeholder="Search for...">
        </div>
    </div>
    <!-- End Search Field -->

    <!-- Navbar -->
    <nav class="navbar mega-menu" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="menu-container">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".nav-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="toggle-icon"></span>
                </button>

                <!-- Navbar Actions -->
                <div class="navbar-actions">
                    <!-- Shopping Cart -->
                    <div class="navbar-actions-shrink shopping-cart hidden">
                        <i class="shopping-cart-icon fa fa-shopping-cart"></i>
                        <ul class="list-unstyled shopping-cart-menu">
                            <li>
                                <span class="shopping-cart-menu-title">Shopping Cart</span>
                            </li>
                            <li class="shopping-cart-menu-content">
                                <div class="shopping-cart-menu-product-media">
                                    <img class="shopping-cart-menu-product-img" src="../../assets/img/250x250/12.jpg" alt="">
                                </div>
                                <div class="shopping-cart-menu-product-wrap">
                                    <a class="shopping-cart-menu-product-name" href="#">Elegand Watch</a>
                                        <span class="shopping-cart-menu-product-price">
                                            <span class="shopping-cart-menu-product-price-sign">&pound;</span>
                                            39.99
                                        </span>
                                </div>
                                <a class="shopping-cart-close" href="javascript:void(0);">&times;</a>
                            </li>
                            <li class="shopping-cart-menu-content">
                                <div class="shopping-cart-menu-product-media">
                                    <img class="shopping-cart-menu-product-img" src="../../assets/img/250x250/13.jpg" alt="">
                                </div>
                                <div class="shopping-cart-menu-product-wrap">
                                    <a class="shopping-cart-menu-product-name" href="#">Elegand Shoes</a>
                                        <span class="shopping-cart-menu-product-price">
                                            <span class="shopping-cart-menu-product-price-sign">&pound;</span>
                                            10.99
                                        </span>
                                </div>
                                <a class="shopping-cart-close" href="javascript:void(0);">&times;</a>
                            </li>
                            <li class="shopping-cart-subtotal">
                                <div class="shopping-cart-subtotal-content">
                                    <span class="shopping-cart-subtotal-title">Subtotal</span>
                                    <span class="shopping-cart-subtotal-price">&pound; 50.98</span>
                                </div>
                                <button type="button" class="btn-base-bg btn-base-sm btn-block radius-3 margin-b-10">Checkout</button>
                                <p class="shopping-cart-subtotal-view">or <a class="shopping-cart-subtotal-view-link" href="#">View shopping cart</a></p>
                            </li>
                        </ul>
                    </div>
                    <!-- End Shopping Cart -->

                    <!-- Search -->
                    <div class="navbar-actions-shrink search">
                        <div class="search-btn">
                            <i class="search-btn-default fa fa-search"></i>
                            <i class="search-btn-active fa fa-times"></i>
                        </div>
                    </div>
                    <!-- End Search -->

                    <!-- Sidebar -->
                    <a class="navbar-actions-shrink sidebar-trigger color-white" href="javascript:void(0);">
                        <i class="fa fa-envelope-o"></i>
                    </a>
                    <!-- End Sidebar -->
                </div>
                <!-- End Navbar Actions -->

                <!-- Logo -->
                <div class="navbar-logo">
                    <a class="navbar-logo-wrap" href="/">
                        <img class="navbar-logo-img" src="{{ asset('assets/img/250x250/kali-icon-white.png') }}" style="width: 50px;" alt="Kali Manila">
                        <span class="h4 color-white hidden" style="">KM</span>
                    </a>
                </div>
                <!-- End Logo -->
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse nav-collapse">
                <div class="menu-container">
                    <ul class="nav navbar-nav">
                        <!-- Home -->
                        <li class="nav-item">
                            <a class="nav-item-child" href="{{ route('about') }}">
                                <i class="pe-7s-info"></i> About
                            </a>
                        </li>
                        <!-- End Home -->

                        <!-- Blog -->
                        <li class="nav-item">
                            <a class="nav-item-child" href="{{ route('blog') }}">
                                <i class="pe-7s-news-paper"></i> Blog
                            </a>
                        </li>
                        <!-- Blog -->

                        <!-- Pages -->
                        <li class="nav-item">
                            <a class="nav-item-child" href="{{ route('training') }}">
                                <i class="pe-7s-gleam"></i> Training
                            </a>
                        </li>
                        <!-- End Pages -->

                        <!-- Features -->
                        <li class="nav-item dropdown">
                            <a class="nav-item-child dropdown-toggle" href="javascript:void(0)" data-toggle="dropdown">
                                <i class="pe-7s-photo-gallery"></i> Gallery
                            </a>
                            <ul class="dropdown-menu">
                                <li class="dropdown-menu-item">
                                    <a class="dropdown-menu-item-child" href="{{ route('gallery') }}"><i class="pe-7s-photo"></i> Photos</a>
                                </li>
                                <li class="dropdown-menu-item">
                                    <a class="dropdown-menu-item-child" href="{{ route('gallery') }}"><i class="pe-7s-play"></i> Videos</a>
                                </li>
                            </ul>
                        </li>
                        <!-- End Features -->

                        <!-- Portfolio -->
                        <li class="nav-item dropdown">
                            <a class="nav-item-child" href="{{ route('faqs') }}">
                                <i class="pe-7s-note2"></i> FAQs
                            </a>
                        </li>
                        <!-- End Portfolio -->

                        <!-- Login -->
                        {{--<li class="nav-item form-modal-nav">--}}
                            {{--<a class="nav-item-child form-modal-login radius-3" href="javascript:void(0);"><i class="fa fa-user"></i></a>--}}
                        {{--</li>--}}
                        <!-- End Login -->
                    </ul>
                </div>
            </div>
            <!-- End Navbar Collapse -->
        </div>
        <!--// End Container-->
    </nav>
    <!-- Navbar -->
</header>