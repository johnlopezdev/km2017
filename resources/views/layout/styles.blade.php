<!-- REVOLUTION SLIDER -->
<link href="http://fonts.googleapis.com/css?family=Raleway:500,800" rel="stylesheet" property="stylesheet" type="text/css" media="all" />
<link href="{{ asset('assets/plugins/rev-slider/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('assets/plugins/rev-slider/fonts/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
<!-- END REVOLUTION SLIDER -->

<!-- GLOBAL MANDATORY STYLES -->
<link href='https://fonts.googleapis.com/css?family=Roboto:100,400,700,500,300,300italic' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
<link href="{{ asset('assets/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/plugins/et-line/et-line.css') }}" rel="stylesheet" type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->

<!-- THEME -->
<link href="{{ asset('assets/css/theme/dark.css') }}" rel="stylesheet" type="text/css"/>

<!-- BEGIN THEME PLUGINS STYLE -->
<link href="{{ asset('assets/plugins/scrollbar/jquery.mCustomScrollbar.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/plugins/magnific-popup/magnific-popup.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/plugins/cubeportfolio/css/cubeportfolio.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/plugins/rev-slider/css/settings.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('assets/plugins/rev-slider/css/layers.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('assets/plugins/rev-slider/css/navigation.css') }}" rel="stylesheet" type="text/css">
<!-- END THEME PLUGINS STYLE -->

<!-- THEME STYLES -->
<link href="{{ asset('assets/css/global.css') }}" rel="stylesheet" type="text/css"/>
<!-- END THEME STYLES -->