@extends('layout.master')
@section('content')

<!--========== REVOLUTION SLIDER ==========-->
@include('pages.welcome.hero')
<!--========== END REVOLUTION SLIDER ==========-->

<!--========== TEASER AREA :: BEGIN ==========-->
<!-- Heading v3 -->
@include('pages.welcome.pointers')
<!-- End Heading v3 -->

<!-- Blog Teaser -->
@include('pages.welcome.teasers')
<!-- End Blog Teaser -->
<!--========== TEASER AREA :: END ==========-->

<div class="content-lg container">
    <div class="row">
        <div class="col-sm-6 sm-margin-b-30">
            <!-- Address -->
            <div class="footer-address">
                <blockquote>
                    <span class="footer-title"><i class="pe-7s-home"></i></span> <br>
                    <p class="footer-address-text">
                        Blue Ridge B basketball Court <br>
                        Comet Loop, Blue Ridge <br>
                        Quezon City, Philippines
                    </p>
                </blockquote>

                <blockquote>
                    <span class="footer-title"><i class="pe-7s-call"></i></span> <br>
                    <p class="footer-address-text">+63 939 910 6450</p>
                </blockquote>

                <blockquote>
                    <span class="footer-title"><i class="pe-7s-mail"></i></span> <br>
                    <p class="footer-address-link">ptkmanila-123-a-z@gmail.com</p>
                </blockquote>
            </div>
            <!-- Address -->
        </div>
        <div class="col-sm-6">
            <!-- Google Map :: change to API soon -->
            @include('layout.scripts.google-map')
            <!-- Google Map -->
        </div>
    </div>
    <!-- end row -->
</div>

@endsection
