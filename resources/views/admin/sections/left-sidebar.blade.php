<nav class="page-sidebar" data-pages="sidebar">
    <!-- BEGIN SIDEBAR MENU TOP TRAY CONTENT-->
    <div class="sidebar-overlay-slide from-top" id="appMenu">
        <div class="p-25">
            <ul class="list-group">
                @for($x = 0; $x < 5; $x++)
                    <li class="list-group-item">
                        <b>May 12, 2017 - <small>10:30pm</small></b>
                        <p>John Doe published a new blog</p>
                    </li>
                @endfor
                <li class="list-group-item">
                    <a href="#" class="btn btn-block btn-primary text-white">See complete logs</a>
                </li>
            </ul>
        </div>
    </div>
    <!-- END SIDEBAR MENU TOP TRAY CONTENT-->
    <!-- BEGIN SIDEBAR MENU HEADER-->
    <div class="sidebar-header">
        <img src="assets/img/logo_white.png" alt="logo" class="brand" data-src="assets/img/logo_white.png" data-src-retina="assets/img/logo_white_2x.png" width="78" height="22">
        <div class="sidebar-header-controls">
            <button type="button" class="btn btn-xs sidebar-slide-toggle btn-link m-l-20" data-pages-toggle="#appMenu"><i class="fa fa-angle-down fs-16"></i>
            </button>
            <button type="button" class="btn btn-link visible-lg-inline" data-toggle-pin="sidebar"><i class="fa fs-12"></i>
            </button>
        </div>
    </div>
    <!-- END SIDEBAR MENU HEADER-->
    <!-- START SIDEBAR MENU -->
    <div class="sidebar-menu">
        <!-- BEGIN SIDEBAR MENU ITEMS-->
        <ul class="menu-items">
            <li class="m-t-10">
                <a href="social.html"><span class="title">Dashboard</span></a>
                <span class="icon-thumbnail"><i class="pg-desktop"></i></span>
            </li>
            <li>
                <a href="javascript:;"><span class="title">Calendar</span>
                    <span class=" arrow"></span></a>
                <span class="icon-thumbnail"><i class="pg-calender"></i></span>
                <ul class="sub-menu">
                    <li class="">
                        <a href="calendar.html">Basic</a>
                        <span class="icon-thumbnail">c</span>
                    </li>
                    <li class="">
                        <a href="calendar_lang.html">Languages</a>
                        <span class="icon-thumbnail">L</span>
                    </li>
                    <li class="">
                        <a href="calendar_month.html">Month</a>
                        <span class="icon-thumbnail">M</span>
                    </li>
                    <li class="">
                        <a href="calendar_lazy.html">Lazy load</a>
                        <span class="icon-thumbnail">La</span>
                    </li>
                    <li class="">
                        <a href="http://pages.revox.io/dashboard/2.1.0/doc/#calendar" target="_blank">Documentation</a>
                        <span class="icon-thumbnail">D</span>
                    </li>
                </ul>
            </li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <!-- END SIDEBAR MENU -->
</nav>