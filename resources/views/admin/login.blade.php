<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title>Pages - Admin Dashboard UI Kit - Lock Screen</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
    <link rel="apple-touch-icon" href="pages/ico/60.png">
    <link rel="apple-touch-icon" sizes="76x76" href="pages/ico/76.png">
    <link rel="apple-touch-icon" sizes="120x120" href="pages/ico/120.png">
    <link rel="apple-touch-icon" sizes="152x152" href="pages/ico/152.png">
    <link rel="icon" type="image/x-icon" href="favicon.ico" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <link href="{{ asset('admin/assets/plugins/pace/pace-theme-flash.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('admin/assets/plugins/bootstrapv3/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('admin/assets/plugins/font-awesome/css/font-awesome.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('admin/assets/plugins/jquery-scrollbar/jquery.scrollbar.css') }}" rel="stylesheet" type="text/css" media="screen" />
    <link href="{{ asset('admin/assets/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" media="screen" />
    <link href="{{ asset('admin/assets/plugins/switchery/css/switchery.min.css') }}" rel="stylesheet" type="text/css" media="screen" />
    <link href="{{ asset('admin/pages/css/pages-icons.css') }}" rel="stylesheet" type="text/css">
    <link class="main-stylesheet" href="{{ asset('admin/pages/css/pages.css') }}" rel="stylesheet" type="text/css" />
    <!--[if lte IE 9]>
    <link href="{{ asset('admin/pages/css/ie9.css') }}" rel="stylesheet" type="text/css" />
    <![endif]-->
    <script type="text/javascript">
        window.onload = function()
        {
            // fix for windows 8
            if (navigator.appVersion.indexOf("Windows NT 6.2") != -1)
                document.head.innerHTML += '<link rel="stylesheet" type="text/css" href="{{ asset('admin/pages/css/windows.chrome.fix.css') }}" />'
        }
    </script>
</head>
<body class="fixed-header ">
<div class="login-wrapper">
    <!-- START Login Background Pic Wrapper-->
    <div class="bg-pic">
        <!-- START Background Pic-->
        <img src="{{ asset('admin/assets/img/f8040a0f8081abc6924d34c9f043ff78.jpg') }}" data-src="{{ asset('admin/assets/img/f8040a0f8081abc6924d34c9f043ff78.jpg') }}" data-src-retina="{{ asset('admin/assets/img/f8040a0f8081abc6924d34c9f043ff78.jpg') }}" alt="" class="lazy">
        <!-- END Background Pic-->
        <!-- START Background Caption-->
        <div class="bg-caption pull-bottom sm-pull-bottom text-white p-l-20 m-b-20 bg-info-darker">
            <h1 class="text-white font-montserrat">
                Kali Manila
            </h1>
            <p class="small">
                Administrator's Panel
            </p>
            <ul class="list-inline m-b-20">
                <li><a href="{{ route('home') }}" class="text-white">Home</a></li>
                <li><a href="{{ route('about') }}" class="text-white">About</a></li>
                <li><a href="{{ route('blog') }}" class="text-white">Blog</a></li>
                <li><a href="{{ route('training') }}" class="text-white">Training</a></li>
            </ul>
        </div>
        <!-- END Background Caption-->
    </div>
    <!-- END Login Background Pic Wrapper-->
    <!-- START Login Right Container-->
    <div class="login-container bg-white">
        <div class="p-l-50 m-l-20 p-r-50 m-r-20 p-t-50 m-t-30 sm-p-l-15 sm-p-r-15 sm-p-t-40">

            <!-- START Login Form -->
            <form id="form-login" class="p-t-15" role="form" method="POST" action="{{ route('login') }}">
            {{ csrf_field() }}
            <!-- START Form Control-->
                <div class="form-group form-group-default {{ $errors->has('email') ? ' has-error' : '' }}">
                    <label>Email Login</label>
                    <div class="controls">
                        <input type="text" name="email" value="{{ old('email') }}" placeholder="Email address" class="form-control" required autofocus>
                    </div>
                </div>
                @if ($errors->has('email'))
                    <label class="error" for="email">
                        {{ $errors->first('email') }}
                    </label>
                @endif
            <!-- END Form Control-->
                <!-- START Form Control-->
                <div class="form-group form-group-default {{ $errors->has('password') ? ' has-error' : '' }}">
                    <label>Password</label>
                    <div class="controls">
                        <input type="password" class="form-control" name="password" placeholder="Credentials" required>
                    </div>
                </div>
                @if ($errors->has('password'))
                    <label class="password" for="password">
                        {{ $errors->first('password') }}
                    </label>
            @endif
            <!-- START Form Control-->
                <div class="row">
                    <div class="col-md-6 no-padding">
                        <div class="checkbox check-success">
                            <input type="checkbox" value="1" id="checkbox1">
                            <label for="checkbox1">Remember me</label>
                        </div>
                    </div>
                    <div class="col-md-6 text-right">
                        <a href="#" class="text-muted small">Forgot password?</a>
                    </div>
                </div>
                <!-- END Form Control-->
                <button class="btn btn-info btn-cons m-t-10" type="submit">Sign in</button>
            </form>
            <!--END Login Form-->
            <div class="pull-bottom sm-pull-bottom">
                <div class="m-b-30 p-r-80 sm-m-t-20 sm-p-r-15 sm-p-b-20 clearfix">

                    <div class="col-sm-12 no-padding m-t-10">
                        <p>
                            <small>
                                <b class="text-danger">IMPORTANT NOTE:</b> This section is strictly for KM Administrators.
                                As a security measure, please bear in mind that
                                <b class="text-danger">the device information of those who land on this page are thoroughly logged</b>.
                                If you somehow discovered this page but you
                                are not authorized, please head back to the main page
                                <a href="{{ route('home') }}"><b>here.</b></a>
                            </small>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Login Right Container-->
</div>

<!-- BEGIN VENDOR JS -->
<script src="assets/plugins/pace/pace.min.js" type="text/javascript"></script>
<script src="assets/plugins/jquery/jquery-1.11.1.min.js" type="text/javascript"></script>
<script src="assets/plugins/modernizr.custom.js" type="text/javascript"></script>
<script src="assets/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="assets/plugins/bootstrapv3/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/plugins/jquery/jquery-easy.js" type="text/javascript"></script>
<script src="assets/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script>
<script src="assets/plugins/jquery-bez/jquery.bez.min.js"></script>
<script src="assets/plugins/jquery-ios-list/jquery.ioslist.min.js" type="text/javascript"></script>
<script src="assets/plugins/jquery-actual/jquery.actual.min.js"></script>
<script src="assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js"></script>
<script type="text/javascript" src="assets/plugins/select2/js/select2.full.min.js"></script>
<script type="text/javascript" src="assets/plugins/classie/classie.js"></script>
<script src="assets/plugins/switchery/js/switchery.min.js" type="text/javascript"></script>
<script src="assets/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<!-- END VENDOR JS -->
<script src="pages/js/pages.min.js"></script>
<script>
    $(function()
    {
        $('#form-login').validate()
    })
</script>
</body>
</html>