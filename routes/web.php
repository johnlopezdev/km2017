<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Home Page
Route::get('/', 'PagesController@index')->name('home');

// About Page
Route::get('about', 'PagesController@about')->name('about');

// Blog Page
Route::get('blog', 'PagesController@blog')->name('blog');
Route::get('blog/{id}', 'PagesController@article')->name('article');

// Training Info Page
Route::get('training', 'PagesController@training')->name('training');

// Gallery Page
Route::get('gallery', 'PagesController@gallery')->name('gallery');

// FAQs
Route::get('faqs', 'PagesController@faqs')->name('faqs');

// Authentication Routes
Route::group(['prefix' => 'admin'], function () {
    Auth::routes();

    Route::get('logout', 'Auth\LoginController@logout');
    Route::get('dashboard', 'HomeController@index');
});
