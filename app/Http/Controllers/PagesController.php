<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    // Home Page
    public function index() {
        return view('welcome');
    }

    // About Page
    public function about() {
        return view('pages.about');
    }

    // Blog Page
    public function blog() {
        return view('pages.blog');
    }

    public function article() {
        return view('pages.article');
    }

    // Training Page
    public function training() {
        return view('pages.training');
    }

    // Gallery Page
    public function gallery() {
        return view('pages.gallery');
    }

    // FAQs Page
    public function faqs() {
        return view('pages.faqs');
    }
}
